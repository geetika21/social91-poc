INTRODUCTION

This is a form which is built with the help of HTML, CSS and JavaScript. It validates all the inputs made in the form through JavaScript,
 user can't submit if the form is not valid. It displays the entered data in the alert box after user clicks submit and when all the data is validated.
 User must open the HTML file first to view the website, and all the files like javascript, css and images should be linked.

TECHNOLOGIES
HTML
CSS
JavaScript
